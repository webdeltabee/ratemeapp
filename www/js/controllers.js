/*Menu Controller*/
app.controller('menuCtrl', ['$scope', function($scope) {

}]);

app.controller('loginCtrl', ['$scope', '$state', 'ApiUrl', '$http', function($scope, $state, ApiUrl, $http) {
    $scope.user = {};
    $scope.onError = false;
    $scope.loader = false;
    $scope.login = function() {
        if ($scope.checkbox) {
        	window.localStorage.setItem('loginDetails', JSON.stringify($scope.user));
        }
        $scope.loader = true;
        $http.post(ApiUrl+'Login', $scope.user)
        .then(
        	function successCallback(response){
        		if (response.data.hasOwnProperty('Name')) {
        			$scope.onError = false;
        			window.localStorage.setItem('userDetails', JSON.stringify(response.data));
        			$scope.loader = false;
        			$state.go('rating');
        		}else{
        			$scope.onError = true;
        			$scope.message = response.data.Message;
        			$scope.loader = false;
        		}
        	},
        	function errorCallback(response){
        		console.log(response)
        	}
        );
        //$state.go('rating');
    }
}]);

app.controller('ratingCtrl', ['$scope', 'myCoordinates','ApiUrl','$http','$timeout','$state', function($scope, myCoordinates, ApiUrl, $http, $timeout, $state) {
	$scope.loader = false;
	var userDetails = JSON.parse(window.localStorage.getItem('userDetails'));
	$scope.userDetails = userDetails;
	$scope.rating = 4;
    $scope.data = {
        rating: 1,
        max: 5
    }

    $scope.RatingDetails = {
    	ID: 0,
    	EmployeeID:userDetails.EmployeeId,
    	CustomerID:0,
    	IsRemoved:false,
        CustomerMaster:{
    		ID: 0,
    		CustomerName:'',
    		Phone:"9009121240",
    		Email:"vchaurasiya4@gmail.com",
    	},
        Rating: $scope.data.rating,
    };

    myCoordinates.getLatLong().success(function(res) {
        $scope.RatingDetails.CustomerMaster.Lattitude = res.lat;
        $scope.RatingDetails.CustomerMaster.Longitude = res.lon;
    });




    $scope.$watch('data.rating', function() {
        $scope.RatingDetails.Rating = $scope.data.rating;
    });

    $scope.setImageUrl = function(ele) {
        var input = angular.element(document.querySelector("#capture"));
        input = input[0];
        $scope.RatingDetails.ServicePictures = [];/**/
        var totalImages = input.files.length; 
        if (totalImages<=3 ) {
        	if (totalImages>0) {
        		var count = 1;

        		for (var i = input.files.length - 1; i >= 0; i--) {
        				fileReader(input.files[i], count);
        				count++;
						
				}
				$scope.ImageError = false;
        		return true;
        	}else{
        		$scope.ImageError = true;
        		$scope.ImageErrorMsg = "Please select at least one image.";
        		return false;
        	}
        	
        }else{
        	$scope.ImageError = true;
        	$scope.ImageErrorMsg = "You can only select maximum 3 images";
        	return false;
        }
        
    }
    function fileReader(file, count){
    	var reader = new FileReader();
			    
		reader.onload = function(e) {
        	var base64Img = e.target.result.split(',');
        	var tempObj = {PictureName: file.name};
        	tempObj.PictureContent = base64Img[1];

        	$scope.RatingDetails.ServicePictures.push(tempObj);
        	var imagepreview = "";
        	if (count==1) {
        		imagepreview = angular.element(document.querySelector("span.file-1"));
        	}else if (count==2) {
        		imagepreview = angular.element(document.querySelector("span.file-2"));
        	}else{
        		imagepreview = angular.element(document.querySelector("span.file-3"));
        	}
        	var ImgData = e.target.result;
        	imagepreview.html('<img src="'+ImgData+'" class="img-preview" style="height:40px"/>');
        	count++;
        }
		reader.readAsDataURL(file);
    }
    $scope.rate = function() {
    	if (angular.isUndefined($scope.ImageError) || angular.isUndefined($scope.RatingDetails.ServicePictures) || $scope.RatingDetails.ServicePictures.length==0) {
    		$scope.ImageError = true;
    		$scope.ImageErrorMsg = "Please select at least one image.";
    		return false;
    	}else{
    		$scope.loader = true;
	    	$http.post(ApiUrl+'CreateCustomerFeedback', $scope.RatingDetails).success(function(response){
	    		
	    		if (response.Status) {
	    			$scope.onSuccess = true;
	    			$scope.onError = false;
	    			$scope.message = "Your Response Submitted Successfully ! ";
	    			$timeout(function(){
	    				$state.reload();
	    			}, 2000);
	    		}else{
	    			$scope.onSuccess = false;
	    			$scope.onError = true;
	    			$scope.message = response.Message;
	    		}
	    		$scope.loader = false;
	    	});
    	}
    	
    	
    }

}]);
